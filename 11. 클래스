1. 클래스
    class 키워드로 정의함
    내용이 없으면 중괄호 생략해도 됨

    class Abc
    class Abc{/**/}

2. 새 오브젝트 만들기
    Abc()
        
3. 생성자
    constructor 키워드 사용
    1개의 primary constructor, 여러개의 secondary constructor를 가질 수 있음
    JVM 사용하는 경우 생성자에 기본값을 넣을 수 있음(함수 부분 참고)

    1. primary constructor
        클래스 헤더에 들어감

        1. 파라미터
            여기에 파라미터를 넣는다고 필드가 생기는건 아님, 그냥 말 그대로 생성 파라미터
            constructor 은 생략가능

                class Person constructor(firstName: String) {/**/}
                class Person (firstName: String) {/**/}

            primary constructor 는 코드조각이 없음
            init 키워드로 생성시 필요한 코드를 추가할 수 있음
            init 코드는 여러개 추가할 수 있음
            init 코드에서 primary constructor 에 전달된파라미터 사용할 수 있음

                class Person(name: String) {
                    init {
                        println("name: $name")
                        println("upperCase: " + name.toUpperCase())
                    }

                    init {
                        println("nameLength: ${name.length}")
                    }
                }
        
        2. 필드
            파라미터 앞에 그대로 var 이라고 쓰면 필드가 됨
                class Person(var name: String) {    //var 을 썼으므로 name 은 필드가 됐다
                    init {
                        println("name: $name")
                        println("upperCase: " + name.toUpperCase())
                    }

                    init {
                        println("nameLength: ${name.length}")
                    }
                }

    2. secondary constructor
        클래스 내용에 들어감
        this 를 사용해 필드에 접근 가능

        1. secondary constructor 만 있을 때
            class Person {
                var name: Int
                constructor(name: String) {
                    this.name = name.length;
                }
            }
        
        2. primary, secondary constructor
            secondary constructor 는 무조건 primary constructor 의 대리자여야 한다.
            (대리자 라고 하니 와닿지 않으므로, 대신 "~을 대신 실행하는 놈" 이라고 이해해도 될 것 같다.)

            init , field 초기화 코드는 primary constructor 의 일부가 된다.

            secondary constructor 의 코드의 첫 줄은 primary constructor 의 내용을 대신 실행하는 코드가 된다(대리자 이므로).
            secondary constructor 의 코드가 실행될 때엔 (init, field init 코드를 포함한)primary constructor 가 이미 실행된 후 이다.

            primary constructor 가 없더라도 암묵적으로 secondary constructor 는 primary constructor를 대신 실행하게 된다.

    3. 생성자 총정리

        각각의 내용 정리
            primary constructor:
                init 코드:
                    primary constructor의 일부가 된다.
                field 초기화:
                    primary constructor의 일부가 된다.

            secondary constructor:
                primary constructor를 대신 실행하고, 그 다음 자신의 코드를 실행한다.

        순서:
            1. secondary 가 primary 의 코드를 대신 실행
            2. (init, field 가 있는)primary 실행
            3. secondary 실행


        테스트 코드:
            //primary constructor 의 필드
            class Rectangle(val width: Int, val height: Int) {
                constructor(size: Int) : this(size, size) {
                    //primary constructor 를 대리해 실행(대신 실행), 그 후 secondary constructor 의 내용을 진행
                    println("secondary constructor, width: $width, height: $height")
                    println("secondary constructor, size: $size")
                    println("secondary constructor, area: $area")
                }

                //init 과 field 코드는 위에서부터 내려가며 일부가 되는듯, 코드 위치를 바꾸니 실행순서도 바뀐다.

                //init 은 primary constructor 의 일부
                init {
                    println("init, New rectangle, $width x $height")
                }

                //field init 은 primary constructor 의 일부
                val area:Int = println("field init, $width * $height").let { width * height }
            }

    4. private constructor
        primary, secondary 생성자를 모두 만들지 않으면 아무 파라미터도 없는 public 생성자를 자동으로 만든다.
        생성자를 숨기기 위해선 아무 파라미터도 없는 primary 생성자를 만들고, 거기에 private 를 붙이면 된다.

        인자 없는 secondary, 인자 있는 primary 생성자도 되는것 같다.

        class Abc private constructor() {/**/}
    
    5. 생성자 사용
        자바랑 똑같지만, new 를 사용하지 않는다.
    
    6. 참고
        https://readystory.tistory.com/124

4. 함수

