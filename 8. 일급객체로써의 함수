1. 일급객체인 함수
    일급객체(https://ko.wikipedia.org/wiki/%EC%9D%BC%EA%B8%89_%EA%B0%9D%EC%B2%B4)
    일급객체는 "매개변수로 넘기기/수정하기/변수에 대입하기" 가 지원된다는 뜻이다.
    kotlin 에서는 함수도 일급객체이다.

2. 함수타입
    (타입, 타입, ...) -> 리턴타입

    여기서 괄호를 생략하거나 하면 안되는 것 같다.
    무조건 (...) -> ... 의 괄호+화살표 형태가 되어야 하는듯

3. 함수타입의 변수를 선언하기

    var a: (String) -> Int

    var b: (Unit) -> Unit   //Unit은 java의 void와 같다

    var c: () -> Unit

    //var d: (Int)   //이건 Int 형 변수가 된다.

4. 익명함수

    함수를 대입하기 위해 익명함수를 정의해야 하는데, 기존 4.함수 정의와 똑같다.
    다만, 이 때엔 함수이름을 생략해야 한다.

    var a: (String) -> Int = fun(input: String): Int {return input.length}
    
    var myFncVar: (String) -> Int = {input -> input.length}

    입력인자 설정을 안써도 된다.
    자동으로 추론한다.

    var myFncVar: (String) -> Int = {input.length}

5. 파라미터로 넘기는 함수

    다음과 같은 함수가 있을 때,

    fun fncParam (a:String, fnc:(String) -> String): String = fnc(a)

    파라미터에 익명함수를 정의해서 넘겨줄 수 있다.

    fncParam("abc", fun(a:String):String {return a.toUpperCase()})

    파라미터에 람다식으로 넘겨줄 수 있다.
    단, 이 때에 중괄호는 생략할 수 없다.

    fncParam("abc", {a -> a.toUpperCase()})

    함수 파라미터가 파라미터 목록중 제일 마지막에 있고 파라미터로 람다식을 사용할 때,
    함수호출식의 괄호에서 함수를 빼서 뒤에 쓸 수 있다.
    다음 식은 위의 식과 동일하다.

    fncParam("abc") { a -> a.toUpperCase()}

    입력 파라미터가 1개인 경우, it 을 사용해서 입력파라미터 부분을 생략할 수 있다.

    fncParam("abc") {it.toUpperCase()}


?. 이상하다
    val a:() -> Unit = when(x) {
        "1" -> { println("1")}
        "2" -> fun(_){println("2")}
        else -> {Unit -> println("$x")}
    }

    val a:() -> Unit = when(x) {
        "1" -> { -> println("1")}
        "2" -> fun(){println("2")}
        else -> {-> println("$x")}
    }